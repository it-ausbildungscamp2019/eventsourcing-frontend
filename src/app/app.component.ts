import {Component, OnDestroy, OnInit} from '@angular/core';
import {WebsocketService} from './service/websocket.service';
import {EventDispatcherService} from './service/event-dispatcher.service';
import {NgRedux} from '@angular-redux/store';
import {AppState} from './redux/app-state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  private title = 'Eventsourcing Frontend';

  constructor(private websocketService: WebsocketService,
              private eventDispatcherService: EventDispatcherService,
              private ngRedux: NgRedux<AppState>) {
  }

  ngOnInit(): void {
    // start websocket service
    this.websocketService.initWebsocket();

    // initialize event dispatchers
    this.eventDispatcherService.initDispatcherWhenWebsocketAvailable();
  }

  ngOnDestroy(): void {
  }


}
