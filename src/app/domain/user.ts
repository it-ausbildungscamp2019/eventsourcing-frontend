/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */

export interface User {
  name: string;
}
