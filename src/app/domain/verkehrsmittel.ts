/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */

export interface Verkehrsmittel {
  vmNummer: number;
  vmArt: string;
  bezeichnung: string;
  fahrtpunkte: string[];
  aktuellePosition: string;
  delay: number;
  arrived?: boolean;
}
