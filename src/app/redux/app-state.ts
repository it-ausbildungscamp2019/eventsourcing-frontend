/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */

import {Verkehrsmittel} from '../domain/verkehrsmittel';
import {User} from '../domain/user';

/**
 * This AppStore interface defines the redux store and therewith the state for the whole app (centralized)!
 *
 * <b>Changes on this app state can only be made via redux' actions (i.e. events / commands).</b>
 * The app state is immutable! So, actions will "mutate the state" by returning a new instance of the app state
 * containing the changes made before; this is done by the reducers.
 */
export interface AppState {
  // current user
  user: User;
  verkehrsmittel: Verkehrsmittel[];
}


export const INITIAL_APP_STATE: AppState = {
  user: {name: '<undefined>'},
  verkehrsmittel: []
};
