/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */

import {Injectable} from '@angular/core';
import {UserEpics} from './user.epics';

@Injectable({
  providedIn: 'root'
})
export class RootEpic {
  constructor(private userEpics: UserEpics) {
  }

  createEpics(): any[] {
    return [
      this.userEpics
    ];
  }
}
