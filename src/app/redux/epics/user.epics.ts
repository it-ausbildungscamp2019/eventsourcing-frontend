/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */

import {Epic as IEpic, ofType} from 'redux-observable';
import {Injectable} from '@angular/core';
import {Epic} from 'redux-observable-decorator';
import {AppState} from '../app-state';
import {Observable} from 'rxjs';
import {UserAction, UserActions} from '../actions/user.actions';
import {WebsocketService} from '../../service/websocket.service';
import {ignoreElements, tap} from 'rxjs/operators';
import {VerkehrsmittelAction, VerkehrsmittelActions} from '../actions/verkehrsmittel.actions';

@Injectable({
  providedIn: 'root'
})
export class UserEpics {
  constructor(private websocketService: WebsocketService) {
  }

  /**
   * Epic oder Side-Effect zum Laden des Usernames via Backend
   */
  @Epic()
  usernameEpic: IEpic<UserAction, UserAction, AppState, void> =
    (action$: Observable<UserAction>): Observable<UserAction> =>
      action$.pipe(
        ofType(UserActions.LOAD_USERNAME),
        tap(() => this.websocketService.send(UserActions.loadUsername())),
        ignoreElements()
      );

  /**
   *  Epic oder Side-Effect zum Laden der Verkehrsmittel via Backend
   */
  @Epic()
  loadVerkehrsmittelEpic: IEpic<UserAction, UserAction, AppState, void> =
    (action$: Observable<UserAction>): Observable<UserAction> =>
      action$.pipe(
        ofType(UserActions.LOAD_VERKEHRSMITTEL),
        tap(() => this.websocketService.send(UserActions.loadVerkehrsmittel())),
        ignoreElements()
      );

  /**
   *  Epic oder Side-Effect um ein Verkehrsmittel zu "verspäten" (via Backend)
   */
  @Epic()
  delayVerkehrsmittelEpic: IEpic<UserAction, UserAction, AppState, void> =
    (action$: Observable<VerkehrsmittelAction>): Observable<VerkehrsmittelAction> =>
      action$;
  // TODO: implement the missing "side-effect" when delaying a "Verkehrsmittel"

}
