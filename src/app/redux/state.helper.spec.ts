/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */

import {StateHelper} from './state.helper';
import {Verkehrsmittel} from '../domain/verkehrsmittel';

describe('StateHelper', () => {
  const vm: Verkehrsmittel = {
    vmNummer: 711,
    vmArt: 'Fanzug',
    bezeichnung: 'Bölsterli',
    fahrtpunkte: ['AA','OL','BN'],
    aktuellePosition: 'OL',
    delay: 0
  };


  describe('isUserLoggedIn', () => {
    it('returns false when no one is logged in', () => {
      // assert
    });
    it('returns true when someone is logged in', () => {
      // assert
    });
  });
});
