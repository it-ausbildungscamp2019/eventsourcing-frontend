/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */

import {AppState} from './app-state';
import {FluxStandardAction} from 'flux-standard-action';
import {Reducer} from 'redux';
import {tassign} from 'tassign';
import {VerkehrsmittelActions} from './actions/verkehrsmittel.actions';
import {UserActions} from './actions/user.actions';

export const rootReducer: Reducer<AppState, FluxStandardAction<any, any>> =
  (state: AppState, action: FluxStandardAction<any, any>): AppState => {
    // simple error handler (console ;-)
    if (action.error) {
      handleError(action.payload);
      return state;
    }
    switch (action.type) {
      // User-Actions
      case UserActions.USERNAME_LOADED:
        return tassign(state, {user: action.payload});

      // Verkehrsmittel-Actions
      // TODO: implement the missing reducer functions to handle "VmCreated" and "VmMoved"
      case VerkehrsmittelActions.VERKEHRSMITTEL_DELAYED:
        return tassign(state, {
          verkehrsmittel: state.verkehrsmittel.map(verkehrsmittel => {
            if (verkehrsmittel.vmNummer === action.meta as number) {
              return tassign(verkehrsmittel, {delay: action.payload as number})
            }
            return verkehrsmittel;
          })
        })
    }
    return state;
  };

function handleError(error: any): void {
  console.error(error);
}
