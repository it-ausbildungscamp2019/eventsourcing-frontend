/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */
import {Component, OnInit} from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {AppState} from '../redux/app-state';
import {Observable} from 'rxjs';
import {Verkehrsmittel} from '../domain/verkehrsmittel';
import {VerkehrsmittelActions} from '../redux/actions/verkehrsmittel.actions';

@Component({
  selector: 'app-verkehrsmittel',
  templateUrl: './verkehrsmittel.component.html',
  styleUrls: ['./verkehrsmittel.component.css']
})
export class VerkehrsmittelComponent implements OnInit {

  @select(['user', 'name']) user$: Observable<String>;
  @select(['verkehrsmittel']) verkehrsmittel$: Observable<Verkehrsmittel[]>;

  constructor(private ngRedux: NgRedux<AppState>) {
  }

  ngOnInit() {
    console.info('verkehrsmittel component started');
    // TODO: load Verkehrsmittel from backend (trigger event stream)
  }

  public delayVerkehrsmittel(vmNummer: number, delay: number): void {
    console.info('delay clicked for: ' + vmNummer + ' delay=' + delay);
    if (delay) {
      this.ngRedux.dispatch(VerkehrsmittelActions.delay(vmNummer, delay));
    }
  }

}
