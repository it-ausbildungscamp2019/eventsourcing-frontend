/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */
import {CommonModule} from '@angular/common';
import {NgModule, Optional, SkipSelf} from '@angular/core';
import {RouterModule} from '@angular/router';

import {throwIfAlreadyLoaded} from './module-import-guard';
// import {AUTH_INTERCEPTOR, AuthModule} from 'esta-webjs-extensions';
import {HttpClient, HttpClientModule} from '@angular/common/http';
// import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
// import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {environment} from '../../environments/environment';
import {DevToolsExtension, NgRedux, NgReduxModule} from '@angular-redux/store';
import {createEpicMiddleware, Epic} from 'redux-observable';
import {applyMiddleware, createStore, Middleware, Store, StoreEnhancer} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import {AppState, INITIAL_APP_STATE} from '../redux/app-state';
import {combineDecoratedEpics} from 'redux-observable-decorator';
import {rootReducer} from '../redux/reducers';
import {RootEpic} from '../redux/epics/root.epic';

// AOT requires an exported function for factories
// export function HttpLoaderFactory(http: HttpClient) {
//   return new TranslateHttpLoader(http);
// }

@NgModule({
  imports: [
    // AuthModule.forRoot(environment.authConfig, environment.authOptions),
    CommonModule,
    HttpClientModule,
    NgReduxModule,
    RouterModule,
    // TranslateModule.forRoot({
    //   loader: {
    //     provide: TranslateLoader,
    //     useFactory: HttpLoaderFactory,
    //     deps: [HttpClient]
    //   }
    // })
  ],
  declarations: [],
  exports: [],
  providers: []
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule,
              private ngRedux: NgRedux<AppState>,
              private reduxDevTools: DevToolsExtension,
              private rootEpic: RootEpic) {

    throwIfAlreadyLoaded(parentModule, 'core module');

    const epicMiddleware = createEpicMiddleware();
    const store: Store<AppState> = createStore(rootReducer, INITIAL_APP_STATE, this.createEnhancer(epicMiddleware));
    const epics: Epic = combineDecoratedEpics(...this.rootEpic.createEpics());
    this.ngRedux.provideStore(store);

    epicMiddleware.run(epics);
  }

  /**
   * Erstellt aus der Middleware einen StoreEnhancer; dabei wird berücksichtigt, ob die Dev-Tools aktiv sind.
   */
  private createEnhancer(middleware: Middleware<{}, AppState, any>): StoreEnhancer<{ dispatch: {} }> {
    const storeEnhancer: StoreEnhancer<{ dispatch: {} }> = applyMiddleware(middleware);
    if (this.reduxDevTools.isEnabled()) {
      return composeWithDevTools(storeEnhancer);
    }
    return storeEnhancer;
  }
}
