/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */
export function throwIfAlreadyLoaded(parentModule: any, moduleName: string) {
  if (parentModule) {
    throw new Error(`${moduleName} has already been loaded. Import core modules in the AppModule only.`);
  }
}

