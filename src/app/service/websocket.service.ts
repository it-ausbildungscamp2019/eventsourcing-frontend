/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */

import {Injectable, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';
import {$WebSocket, WebSocketSendMode} from 'angular2-websocket/angular2-websocket';
import Timer = NodeJS.Timer;

/** Service, welcher den Zugriff auf eine Websocket-Verbindung zum Server ermöglicht und auch autom. ein
 *  Reconnect durchführt, sollte die Connection "unerwartet" geschlossen werden (z.B. Netzwerkfehler o.ä.):
 *  dabei kann man sich auf die eingehenden Meldungen, Errors oder Close-Events abonnieren (Observable)
 *  und auch selber Daten ans Backend senden (via send() )
 */
@Injectable({
  providedIn: 'root'
})
export class WebsocketService implements OnDestroy {

  private websocket$: $WebSocket;
  private timer: Timer;

  constructor(private router: Router) {
  }

  ngOnDestroy(): void {
    // cancel keep-alive timer and force close the socket
    // clearInterval(this.timer);
    this.websocket$.close(true);
  }

  public setOnOpenCallbackAndGetDataStream(onOpenCallback: any): Subject<any> {
    // register (re)connect callback handler
    this.websocket$.onOpen(onOpenCallback);
    return this.websocket$.getDataStream();
  }

  public send(data: any): void {
    // send data in "direct" mode (no promise or observable as result)
    this.websocket$.send(JSON.stringify(data), WebSocketSendMode.Direct);
  }

  /**
   * Methode initialisiert den (einzigen) Websocket dieser Browser-session
   * Methode wird aus 'AppComponent->ngOnInit' aufgerufen, aber erst wenn
   * 'authService->authenticated()' = true ist (und somit ein valider 'oauth2Token'
   *  zur Verfügung steht)
   *
   * @param oauth2Token Das Autorisierungstoken
   */
  public initWebsocket(): void {
    // const subprotocols: string[] = [WebsocketService.REDKO_WS_SUBPROTOCOL_DUMMY, oauth2Token, environment.serviceName];
    const url: string = environment.backendUrl;

    this.websocket$ = new $WebSocket(url, [] ,  {
      initialTimeout: 5000,
      maxTimeout: 60000,
      reconnectIfNotNormalClose: true
    });
    // this.timer = setInterval(() => this.send({type: `keep-alive`}), 100000);

    this.websocket$.onError((ev: Event) => this.cbErrorFromWebsocket(ev));
  }

  private cbErrorFromWebsocket(ev: Event): void {
    console.error(ev);
    this.router.navigate(['/error'], {queryParams: {msgkey: 'websocket_error_common'}});
  }
}
