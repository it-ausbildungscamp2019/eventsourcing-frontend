/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
 */
import {Injectable, OnDestroy} from '@angular/core';
import {AppState} from '../redux/app-state';
import {NgRedux} from '@angular-redux/store';
import {WebsocketService} from './websocket.service';
import {Subject, Subscription} from 'rxjs';
import {FluxStandardAction} from 'flux-standard-action';
import {UserActions} from '../redux/actions/user.actions';

@Injectable({
  providedIn: 'root'
})
export class EventDispatcherService implements OnDestroy {

  private eventstream: Subject<any>;
  private subscription: Subscription;

  constructor(private ngRedux: NgRedux<AppState>,
              private websocketService: WebsocketService) {
  }

  ngOnDestroy(): void {
    console.log('event dispatcher service stopped');
    // cancel the subscription and close websocket
    this.subscription.unsubscribe();
    this.eventstream.complete();
  }

  public initDispatcherWhenWebsocketAvailable(): void {
    console.log('event dispatcher service started');

    this.eventstream = this.websocketService.setOnOpenCallbackAndGetDataStream(() => this.onReconnect());
    this.subscribeToEvents();
  }

  private onReconnect(): void {
    // trigger user name from backend
    this.ngRedux.dispatch(UserActions.loadUsername());
  }

  private subscribeToEvents(): void {
    this.subscription = this.eventstream.subscribe(
      event => this.handleEvent(JSON.parse(event.data)),
      error => EventDispatcherService.handleError(error),
      () => EventDispatcherService.handleOnComplete()
    );
  }

  private static handleOnComplete(): void {
    console.warn(`websocket closed`);
  }

  private static handleError(error: any): void {
    console.error('websocket error received:', error);
  }

  private handleEvent(event: FluxAction): void {
    if (!event) {
      return;
    }
    switch (event.type) {
      case UserActions.USERNAME_LOADED:
        this.ngRedux.dispatch(UserActions.usernameLoaded(event.payload));
        break;
      // TODO: implement the rest of the case statements for events arrived from backend
      // Fallback
      default:
        console.error('unhandled event received:', event);
        break;
    }
  }
}

/**
 * Interface für Actions, die das Backend ans Frontend schickt.
 */
interface FluxAction extends FluxStandardAction<any, any> {
  type: string;
  payload: any;
  meta: any;
  error: boolean;
}
