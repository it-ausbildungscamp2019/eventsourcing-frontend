## SBB IT-Ausbildungscamp 2019 - "Hands-On Eventsourcing and CQRS using reactive programming"

### Content
Here you'll find the solutions for the main example (frontend) using angular with redux (event sourcing in ui).

* `websocketservice` - angular2 compliant websocket service which uses the rx.js websocket subject and observable.
  it wil be started by eventdispatcher when the main component gets loaded
* `eventdispatcher` - event dispatcher service where all traffic from backend to client flows through. It maps
   the incoming JSON event(S) according to their types and dispatches them to the redux reducer / store.
* `verkehrsmittel` - angular component which receives the event stream of verkehrsmittel entities from backend and 
   shows them (incl. live updates and so om)
* `redux store` - the one and only place where app state should remain! changes to this state can only be made via events
   aka. "actions". so, one-way data binding and "on-push"-change detection are all we need in this scenario.
* `redux reducers` - all "actions" (events) are dispatched to one of the reducer: it takes the current state and
   the action just triggered to reduce them to one single result, the new app state (new instance of it)
* `redux epics or sideeffects` - from time to time, it' not enough to simply dispatch an action to a reducer which
   mutates the app state and fine. But in some cases we need to do a bit more, maybe trigger another action 
   or we send an action to the backend in order to load some more data etc.
